#include "clock.h"

Clock::Clock()
{
  mLastTransition = 0;
  mPeriod = 0;
  mHigh = 0;
}

Clock::~Clock() {}

int Clock::update(unsigned long currentTime)
{
  if ((currentTime - mLastTransition) > (mPeriod / 2)) {
    mLastTransition = mLastTransition + mPeriod / 2;
    mHigh = !mHigh;
  }
}

char Clock::isHigh()
{
  return mHigh;
}

int Clock::setCurrentTime(unsigned long currentTime)
{
  mLastTransition = currentTime;
}

int Clock::setPeriod(int period)
{
  mPeriod = period;
}
