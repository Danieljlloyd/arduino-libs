#ifndef _CLOCK_T
#define _CLOCK_T

class Clock {
  public:
    Clock();
    ~Clock();
    int setCurrentTime(unsigned long currentTime);
    int setPeriod(int period);
    int update(unsigned long currentTime);
    char isHigh();

  private:
    unsigned long mLastTransition;
    char mHigh;
    int mPeriod;
};

#endif
